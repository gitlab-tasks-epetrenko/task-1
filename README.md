- Script.sh - takes userlist, tracks md5sum and writes out to log if userlist and their home folder is changed

- crontab - entry to run this script hourly

Sources used:
- info bash
- man bash - to double check some if conditions 
- man 5 crontab - to confirm fields and syntax
