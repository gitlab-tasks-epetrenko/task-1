#!/bin/bash

# get list of users in requested format and write it down to temporary file
PWDRESULTS=`cat /etc/passwd | cut -d : -f 1,6 > /tmp/curusers_out.tmp`

# get md5 of the file
MD5=`md5sum /tmp/curusers_out.tmp`
echo $MD5 > /tmp/nowrun

# There are no files on the first run, we are checking if the required files are exist
if [[ -e /var/log/current_users ]]; then
	md5sum --check --quiet --status /var/log/current_users;
	# checking results, if it's non-zero - md5 doesnt match
	if [[ $? != 0 ]]; then
		# write down user_changes
		echo `date -u "+%Y.%m.%d %H:%M:%S"`' changes occured' >> /var/log/user_changes
		# replace MD5 hash
		echo $MD5 > /var/log/current_users
		echo "Changes logged"
	fi
	echo "MD5 not changed"
else
	echo $MD5 > /var/log/current_users;
	echo "Writing MD5 on the first run"
fi
